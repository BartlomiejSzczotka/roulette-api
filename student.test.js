const http = require('http');
const rp = require('request-promise');
const apiUrl = 'https://roulette-api.nowakowski-arkadiusz.com';

// Helper to make the code shorter
const post = (path, hashname = '', body = {}) => rp({
    method: 'POST',
    uri: apiUrl + path,
    body: body,
    json: true,
    headers: { 'Authorization': hashname }
});

// Helper to make the code shorter
const get = (path, hashname = '') => rp({
    method: 'GET',
    uri: apiUrl + path,
    json: true,
    headers: { 'Authorization': hashname }
});

//my check
//test 0 - black
for (let number of [0]) {
    test('test black: ' + number + ' is spinned', () => {
        return post('/players')
        .then(response => {
            hashname = response.hashname;
            return post('/bets/black', hashname, { chips: 100 }); // a bet
        })
        .then(response => post('/spin/' + number, hashname)) // spin the wheel
        .then(response => get('/chips', hashname)) // checking the number of chips
        .then(response => expect(response.chips).toEqual(0))
    });
}

//test 0 - red
for (let number of [0]) {
    test('test red: ' + number + ' is spinned', () => {
        return post('/players')
        .then(response => {
            hashname = response.hashname;
            return post('/bets/red', hashname, { chips: 100 }); // a bet
        })
        .then(response => post('/spin/' + number, hashname)) // spin the wheel
        .then(response => get('/chips', hashname)) // checking the number of chips
        .then(response => expect(response.chips).toEqual(0))
    });
}
//even
for (let number of [0]) {
    test('test even: ' + number + ' is spinned', () => {
        return post('/players')
        .then(response => {
            hashname = response.hashname;
            return post('/bets/even', hashname, { chips: 100 }); // a bet
        })
        .then(response => post('/spin/' + number, hashname)) // spin the wheel
        .then(response => get('/chips', hashname)) // checking the number of chips
        .then(response => expect(response.chips).toEqual(0))
    });
}
//odd
for (let number of [0]) {
    test('test odd: ' + number + ' is spinned', () => {
        return post('/players')
        .then(response => {
            hashname = response.hashname;
            return post('/bets/odd', hashname, { chips: 100 }); // a bet
        })
        .then(response => post('/spin/' + number, hashname)) // spin the wheel
        .then(response => get('/chips', hashname)) // checking the number of chips
        .then(response => expect(response.chips).toEqual(0))
    });
}

//black number
for (let number of [2,4,6,8,10,11,13,15,17,19,20,22,24,26,29,31,33,35]) {
    test('Black should double the number of chips if number ' + number + ' is spinned', () => {
        return post('/players')
        .then(response => {
            hashname = response.hashname;
            return post('/bets/black', hashname, { chips: 100 }); // a bet
        })
        .then(response => post('/spin/' + number, hashname)) // spin the wheel
        .then(response => get('/chips', hashname)) // checking the number of chips
        .then(response => expect(response.chips).toEqual(200))
    });
}

//red number
for (let number of [1,3,5,7,9,12,14,16,18,21,23,25,27,28,30,32,34,36]) {
    test('Red should double the number of chips if number ' + number + ' is spinned', () => {
        return post('/players')
        .then(response => {
            hashname = response.hashname;
            return post('/bets/red', hashname, { chips: 100 }); // a bet
        })
        .then(response => post('/spin/' + number, hashname)) // spin the wheel
        .then(response => get('/chips', hashname)) // checking the number of chips
        .then(response => expect(response.chips).toEqual(200))
    });
}


test.each([1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35])('Bet on odds should double the number of chips if number %d is spinned', (number) => {
    return post('/players')
    .then(response => {
        hashname = response.hashname;
        return post('/bets/odd', hashname, { chips: 100 }); // a bet
    })
    .then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(200))
});

test.each([2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36])('Bet on even should double the number of chips if number %d is spinned', (number) => {
    return post('/players')
    .then(response => {
        hashname = response.hashname;
        return post('/bets/even', hashname, { chips: 100 }); // a bet
    })
    .then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(200))
});

//column 1
for (let number of [1,4,7,10,13,16,19,22,25,28,31,34]) {
    test('Column 1: ' + number + ' is spinned', () => {
        return post('/players')
        .then(response => {
            hashname = response.hashname;
            return post('/bets/column/1', hashname, { chips: 100 }); // a bet
        })
        .then(response => post('/spin/' + number, hashname)) // splin the wheel
        .then(response => get('/chips', hashname)) // checking the number of chips
        .then(response => expect(response.chips).toEqual(300))
    });
}

//column 2
for (let number of [2,5,8,11,14,17,20,23,26,29,32,35]) {
    test('Column 2: ' + number + ' is spinned', () => {
        return post('/players')
        .then(response => {
            hashname = response.hashname;
            return post('/bets/column/2', hashname, { chips: 100 }); // a bet
        })
        .then(response => post('/spin/' + number, hashname)) // splin the wheel
        .then(response => get('/chips', hashname)) // checking the number of chips
        .then(response => expect(response.chips).toEqual(300))
    });
}

//column 3
for (let number of [3,6,9,12,15,18,21,24,27,30,33,36]) {
    test('Column 3: ' + number + ' is spinned', () => {
        return post('/players')
        .then(response => {
            hashname = response.hashname;
            return post('/bets/column/3', hashname, { chips: 100 }); // a bet
        })
        .then(response => post('/spin/' + number, hashname)) // splin the wheel
        .then(response => get('/chips', hashname)) // checking the number of chips
        .then(response => expect(response.chips).toEqual(300))
    });
}

//corner
test.each([
    ['1-2-4-5', 1], ['1-2-4-5', 2],['1-2-4-5', 4],['1-2-4-5', 5],
    ['2-3-5-6', 2],['2-3-5-6', 3],['2-3-5-6', 5],['2-3-5-6', 6],
    ['4-5-7-8', 4],['4-5-7-8', 5], ['4-5-7-8', 7],['4-5-7-8', 8],
    ['5-6-8-9', 5],['5-6-8-9', 6],['5-6-8-9', 8],['5-6-8-9', 9],
    ['7-8-10-11', 7],['7-8-10-11', 8],['7-8-10-11', 10],['7-8-10-11', 11],
    ['8-9-11-12', 8],['8-9-11-12', 9],['8-9-11-12', 11],['8-9-11-12', 12],
    ['10-11-13-14', 10],['10-11-13-14', 11],['10-11-13-14', 13],['10-11-13-14', 14],
    ['11-12-14-15', 11],['11-12-14-15', 12],['11-12-14-15', 14],['11-12-14-15', 15],
    ['13-14-16-17', 13],['13-14-16-17', 14],['13-14-16-17', 16],['13-14-16-17', 17],
    ['14-15-17-18', 14],['14-15-17-18', 15],['14-15-17-18', 17],['14-15-17-18', 18],
    ['16-17-19-20', 16],['16-17-19-20', 17],['16-17-19-20', 19],['16-17-19-20', 20],
    ['17-18-20-21', 17],['17-18-20-21', 18],['17-18-20-21', 20],['17-18-20-21', 21],
    ['19-20-22-23', 19],['19-20-22-23', 20],['19-20-22-23', 22],['19-20-22-23', 23],
    ['20-21-23-24', 20],['20-21-23-24', 21],['20-21-23-24', 23],['20-21-23-24', 24],
    ['22-23-25-26', 22],['22-23-25-26', 23],['22-23-25-26', 25],['22-23-25-26', 26],
    ['23-24-26-27', 23],['23-24-26-27', 24],['23-24-26-27', 26],['23-24-26-27', 27],
    ['25-26-28-29', 25],['25-26-28-29', 26],['25-26-28-29', 28],['25-26-28-29', 29],
    ['26-27-29-30', 26],['26-27-29-30', 27],['26-27-29-30', 29],['26-27-29-30', 30],
    ['28-29-31-32', 28],['28-29-31-32', 29],['28-29-31-32', 31],['28-29-31-32', 32],
    ['29-30-32-33', 29],['29-30-32-33', 30],['29-30-32-33', 32],['29-30-32-33', 33],
    ['31-32-34-35', 31],['31-32-34-35', 32],['31-32-34-35', 34],['31-32-34-35', 35],
    ['32-33-35-36', 32],['32-33-35-36', 33],['32-33-35-36', 35],['32-33-35-36', 36],
    ['0-1-2-3', 0],['0-1-2-3', 1],['0-1-2-3', 2],['0-1-2-3', 3]
  ])(
    'Corner x8, bet: %s number: %d',
    (bet, number) =>
      post('/players')
        .then((response) => {
          hashname = response.hashname;
          return post(`/bets/corner/${bet}`, hashname, { chips: 100 }); // a bet
        })
        .then((response) => post('/spin/' + number, hashname)) // spin the wheel
        .then((response) => get('/chips', hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(900))
  );

//dozen

for (let number of [1,2,3,4,5,6,7,8,9,10,11,12]) {
    test('Dozen1: ' + number + ' is spinned', () => {
        return post('/players')
        .then(response => {
            hashname = response.hashname;
            return post('/bets/dozen/1', hashname, { chips: 100 }); // a bet
        })
        .then(response => post('/spin/' + number, hashname)) // splin the wheel
        .then(response => get('/chips', hashname)) // checking the number of chips
        .then(response => expect(response.chips).toEqual(300))
    });
}

for (let number of [13,14,15,16,17,18,19,20,21,22,23,24]) {
    test('Dozen2 ' + number + ' is spinned', () => {
        return post('/players')
        .then(response => {
            hashname = response.hashname;
            return post('/bets/dozen/2', hashname, { chips: 100 }); // a bet
        })
        .then(response => post('/spin/' + number, hashname)) // splin the wheel
        .then(response => get('/chips', hashname)) // checking the number of chips
        .then(response => expect(response.chips).toEqual(300))
    });
}

for (let number of [25,26,27,28,29,30,31,32,33,34,35,36]) {
    test('Dozen3 ' + number + ' is spinned', () => {
        return post('/players')
        .then(response => {
            hashname = response.hashname;
            return post('/bets/dozen/3', hashname, { chips: 100 }); // a bet
        })
        .then(response => post('/spin/' + number, hashname)) // splin the wheel
        .then(response => get('/chips', hashname)) // checking the number of chips
        .then(response => expect(response.chips).toEqual(300))
    });
}

